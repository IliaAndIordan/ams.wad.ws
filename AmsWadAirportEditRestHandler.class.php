<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsWadAirportEditRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                  : IordIord
 * Date Creation			: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsWadAirportEditRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("AmsWadConnection.php");
require_once("AmsWadLogger.php");
require_once("WadAirportEdit.class.php");
require_once("SimpleRest.class.php");
require_once 'Response.class.php';
//require_once("SolrClient.class.php");

// <editor-fold defaultstate="collapsed" desc="AmsWadAirportEditRestHandler Class">

/**
 * Description of AmsWadAirportEditRestHandler class
 *
 * @author IordIord
 */
class AmsWadAirportEditRestHandler extends SimpleRest {

     public function Option() {
        $mn = "AmsWadAirportEditRestHandler::Option()";
        $response = new Response("success", "Service working.");
        
        $rh = new AmsWadAirportEditRestHandler();
        $rh->EncodeResponce($response);
    }
    
    // <editor-fold defaultstate="collapsed" desc="WadAirportEdit Methods">

    public function CountryStateStatusAirportCount() {
        $mn = "AmsWadAirportEditRestHandler::CountryStateStatusAirportCount()";
        AmsWadLogger::logBegin($mn);
        $response = array();
        
        try {
            $response = WadAirportEdit::CountryStateStatusAirportCount();
            
            if (empty($response)) {
                $response = array("status" => "success", "data" => array(), "message" => "No airports found with ICAO code " . $icao);
            }
        } catch (Exception $ex) {
            $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
        }
        AmsWadLogger::log($mn, "sizeof(response)=" . sizeof($response));
        AmsWadLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    

    // </editor-fold>
    
    
}

// </editor-fold>
