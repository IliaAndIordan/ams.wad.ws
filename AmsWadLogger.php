<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $AmsWadLogger;

require_once("LoggerBase.php");
Logger::configure(dirname(__FILE__).'/appender_pdo.properties');
/**
 * Description of Log
 *
 * @author izior
 */
class AmsWadLogger extends LoggerBase {
    
    // <editor-fold defaultstate="collapsed" desc="__construct">
    public function __construct() {
        parent::__construct();
        $this->MN = "AmsWadLogger: ";
        try {
            
            
            $this->logger = Logger::getRootLogger();
           
            $this->logger->debug("AmsWadLogger init");
           
        } catch (Exception $ex) {
            echo "AmsWadLogger Error: " . $ex."<br/>";
        }
        //logEndST($MN, $ST);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function loggerWad(){
         global $AmsWadLogger;
        if(!isset($AmsWadLogger))
        {
            $AmsWadLogger = new AmsWadLogger();
        }
        
        return $AmsWadLogger;
    }
    
    public static function currLogger(){
         global $AmsWadLogger;
        if(!isset($AmsWadLogger))
        {
            $AmsWadLogger = new AmsWadLogger();
        }
        
        return $AmsWadLogger;
    }
    
    public static function logBegin($mn)
    {
        AmsWadLogger::loggerWad()->begin($mn);
    }
    
     public static function logEnd($mn)
    {
        AmsWadLogger::loggerWad()->end($mn);
    }
    
     public static function log($mn, $msg)
    {
        AmsWadLogger::loggerWad()->debug($mn,$msg);
    }
    
    public static function logError($mn, $ex)
    {
        AmsWadLogger::loggerWad()->error($mn);
    }
    // </editor-fold>
}
