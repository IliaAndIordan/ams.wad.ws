<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: AmsWadCountryRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                  : IordIord
 * Date Creation			: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: AmsWadCountryRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("AmsWadConnection.php");
require_once("AmsWadLogger.php");
require_once("WadCountry.class.php");
require_once("SimpleRest.class.php");
require_once 'Response.class.php';
//require_once("SolrClient.class.php");

// <editor-fold defaultstate="collapsed" desc="AmsWadCountryRestHandler Class">

/**
 * Description of AmsWadCountryRestHandler class
 *
 * @author IordIord
 */
class AmsWadCountryRestHandler extends SimpleRest {

    
    // <editor-fold defaultstate="collapsed" desc="WAD Country Base Methods">

    public function CountyAll() {
        $mn = "AmsWadCountryRestHandler::CountyAll()";
        AmsWadLogger::logBegin($mn);
        $response = array();
        
        try {
            $response = WadCountry::loadAll();
            
            if (empty($response)) {
                $response = array("status" => "success", "data" => array(), "message" => "No airports found with ICAO code " . $icao);
            }
        } catch (Exception $ex) {
            $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
        }
        AmsWadLogger::log($mn, "sizeof(response)=" . sizeof($response));
        AmsWadLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function County($id) {
        $mn = "AmsWadCountryRestHandler::County(".$id.")";
        AmsWadLogger::logBegin($mn);
        $response = array();
        
        try {
            $country = new WadCountry();
            $country = $country->loadById($id);
            array_push($response, $country);
            
            if (empty($response)) {
                $response = array("status" => "success", "data" => array(), "message" => "No airports found with ID code " . $id);
            }
        } catch (Exception $ex) {
            $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
        }
        AmsWadLogger::log($mn, "sizeof(response)=" . sizeof($response));
        AmsWadLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CountyByIso2($iso2) {
        $mn = "AmsWadCountryRestHandler::CountyByIso2(".$iso2.")";
        AmsWadLogger::logBegin($mn);
        $response = array();
        
        try {
            
            $response = WadCountry::loadByIso2($iso2);
            
            if (empty($response)) {
                $response = array("status" => "success", "data" => array(), "message" => "No airports found with ISO 2 code " . $iso2);
            }
        } catch (Exception $ex) {
            $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
        }
        AmsWadLogger::log($mn, "sizeof(response)=" . sizeof($response));
        AmsWadLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CountyByRegion($region) {
        $mn = "AmsWadCountryRestHandler::CountyByRegion(".$region.")";
        AmsWadLogger::logBegin($mn);
        $response = array();
        
        try {
            
            $response = WadCountry::loadByRegion($region);
            
            if (empty($response)) {
                $response = array("status" => "success", "data" => array(), "message" => "No airports found in Continent/Region " . $region);
            }
        } catch (Exception $ex) {
            $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
        }
        AmsWadLogger::log($mn, "sizeof(response)=" . sizeof($response));
        AmsWadLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     public function CountyBySubRegion($region) {
        $mn = "AmsWadCountryRestHandler::CountyByRegion(".$region.")";
        AmsWadLogger::logBegin($mn);
        $response = array();
        
        try {
            
            $response = WadCountry::loadBySubRegion($region);
            
            if (empty($response)) {
                $response = array("status" => "success", "data" => array(), "message" => "No airports found in Sub Region  " . $region);
            }
        } catch (Exception $ex) {
            $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
        }
        AmsWadLogger::log($mn, "sizeof(response)=" . sizeof($response));
        AmsWadLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function Region() {
        $mn = "WAdminRestHandler::Region()";
        AmsWadLogger::logBegin($mn);
        $response = new Response();

        $sql = "SELECT region, region_code, count(*) as countries, 
                count(distinct(sub_region)) as sub_regions
                FROM iordanov_ams_wad.cfg_country
                group by region
                order by region";
        try {
            $conn = AmsWadConnection::dbConnect();
            $logModel = AmsWadLogger::loggerWad()->getModule($mn);
            $ret_regions = $conn->dbExecuteSQLJson($sql, $logModel);
            //$bound_params_r = ["s", $iso2];
            //$response = $conn->SelectJson($strSQL, $bound_params_r);
            
            //AmsWadLogger::log($MN, "ret_regions=" . prArr($ret_regions));
            if (isset($ret_regions) && count($ret_regions) > 0) {
                $response->data = $ret_regions;
            }
            else
            {
                $response = array("status" => "success", "data" => array(), "message" => "No regions data found.");
            }
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        //AmsWadLogger::log($mn, " response = " . $response->toJSON());
        AmsWadLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function RegionByName($name) {
        $mn = "WAdminRestHandler::Region()";
        AmsWadLogger::logBegin($mn);
        $response = new Response();

        $sql = "SELECT region, region_code, count(*) as countries, 
                count(distinct(sub_region_code)) as sub_regions
                FROM iordanov_ams_wad.cfg_country
                where region=?
                group by region ";
        try {
            $conn = AmsWadConnection::dbConnect();
            $logModel = AmsWadLogger::loggerWad()->getModule($mn);
            //$ret_regions = $conn->dbExecuteSQLJson($sql, $logModel);
            $bound_params_r = ["s", $name];
            $ret_regions = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("region",$ret_regions);
            
            $sql = "SELECT c.sub_region, c.sub_region_code, 
                count(distinct(c.country_id)) as countries, 
                count(distinct(st.state_id)) as states
                FROM iordanov_ams_wad.cfg_country c
                join iordanov_ams_wad.cfg_country_state st on st.country_id = c.country_id
                where c.region=? 
                group by c.sub_region, c.region ";
            $ret_sub_region = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("sub_regions",$ret_sub_region);
            
            //AmsWadLogger::log($MN, "ret_regions=" . prArr($ret_regions));
//            if (isset($ret_regions) && count($ret_regions) > 0) {
//                $response->data = $ret_regions;
//            }
//            else
//            {
//                $response = array("status" => "success", "data" => array(), "message" => "No regions data found.");
//            }
            
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        //AmsWadLogger::log($mn, " response = " . $response->toJSON());
        AmsWadLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

    // </editor-fold>
    
    
}

// </editor-fold>

//function prArr($tmpArray) {
//    $mn = "prArr()";
//    AmsWadLogger::logBegin($mn);
//
//    foreach ($tmpArray as $number_variable => $variable) {
//
//        if (!is_array($variable) && !is_array($number_variable)) {
//            AmsWadLogger::log($mn, "[" . $number_variable . "] " . $variable);
//        }
//    }
//    AmsWadLogger::logEnd($mn);
//}