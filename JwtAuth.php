<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : api.account    
 *  Date Creation  : Apr 6, 2018 
 *  Filename           : JwtAuth.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */
require_once("ams.config.inc.php");
require_once("JWT.php");
require_once("AmsWadLogger.php");

/**
 * Description of JwtAuth
 *
 * @author IZIordanov
 */
class JwtAuth {

    public static function signTocken($user_id, $user_name) {
        $retValue = false;
        if (!isset($user_id))
            return false;
        $mn = "JwtAuth::CreateTocken('.$user_id.')";
        AmsWadLogger::logBegin($mn);

        $jsonTokenId = $user_id; //SECRET_SERVER_KEY; //base64_encode(mcrypt_create_iv(32));
        $issuedAt = time();
        $notBefore = $issuedAt + 10; //Adding 10 seconds
        $expire = $notBefore + 3600; // Adding 1 hour
        $issuer = SEVER_NAME;   // Retrieve the server name from config file
        $key = SECRET_SERVER_KEY;
        /*
         * Create the token  payload as an array
         */
        $payload = [
            'iat' => $issuedAt, // Issued at: time when the token was generated
            'jti' => $jsonTokenId, // Json Token Id: an unique identifier for the token
            'iss' => $issuer, // Issuer
            'nbf' => $notBefore, // Not before
            'exp' => $expire, // Expire
            'data' => [// Data related to the signer user
                'user_id' => $user_id, // userid from the users table
                'user_name' => $user_name, // User name
            ]
        ];
        AmsWadLogger::log($mn, "iat:" . $payload->iat . ", exp:" . $payload->iat . "nbf:" . $payload->nbf);
        $token = JWT::encode($payload, $key);
        //AmsWadLogger::log($mn, "token = " . $token);
        try {
            $payload_dec = Jwt::decode($token, $key);
            if (isset($payload_dec)) {
                //AmsWadLogger::log($mn, "payload_dec.data.user_id = " . $payload_dec->data->user_id);
                header('X-Authorization:' . $token);
                //header('Authorization: ' . $token);
                $retValue = true;
            }
        } catch (BeforeValidException $e) {
            AmsWadLogger::logError($mn, $ex);
        }

        AmsWadLogger::logEnd($mn);
        return $retValue;
    }

    /**
     * Check Bearer Token and if valid return payload
     * 
     * @return payload decoded tocken payload if valud or null if not
     */
    public static function Autenticate() {
        $mn = "JwtAuth::Autenticate()";
        AmsWadLogger::logBegin($mn);

        $result = new AutenticateResult();
        $result->isValud = false;
        $result->message = "Unexpected error during token validation.";

        $payload = null;

        $token = JwtAuth::getBearerToken();
        $key = SECRET_SERVER_KEY;

        if (isset($token)) {
            AmsWadLogger::log($mn, "token = " . $token);

            try {
                $payload = Jwt::decode($token, $key);
                if (isset($payload)) {
                    $result->payload = $payload;
                    
                    // jti: Json Token Id: an unique identifier for the token
                    AmsWadLogger::log($mn, "payload.data.user_id = " . $payload->data->user_id . ", jti: " . $payload->jti);
                    if ($payload->data->user_id != $payload->jti) {
                        $result->isValud = false;
                        $result->message = 'Token Invalid! Error: User does not corespond to token. Please, authenticate again.';
                    } else if (JwtAuth::isTokenExpired($payload)) {
                        $result->isValud = false;
                        $result->message = "Token Invalid! Error: Token expired. Please, refresh your token.";
                    } else {
                        $result->isValud = true;
                        $result->message = "Token is valid.";
                    }
                }
            } catch (BeforeValidException $e) {
                AmsWadLogger::logError($mn, $ex);
                $result->isValud = true;
                $result->message = $ex;
            }
        }
        AmsWadLogger::logEnd($mn);
        return $result;
    }

    /**
     * Check Bearer Token and if valid return payload
     * 
     * @return payload decoded tocken payload if valud or null if not
     */
    public static function RefreshTocken() {
        $mn = "JwtAuth::RefreshTocken()";
        $retValue = false;
        AmsWadLogger::logBegin($mn);
        $payload = JwtAuth::Autenticate();

        $key = SECRET_SERVER_KEY;
        if (isset($payload)) {
            AmsWadLogger::log($mn, "user = " . $payload->data->user_id);
            $retValue = JwtAuth::signTocken($payload->data->user_id, $payload->data->user_name);
        }
        AmsWadLogger::logEnd($mn);
        return $retValue;
    }

    /**
     * Get hearder Authorization
     * */
    public static function getAuthorizationHeader() {
        $headers = null;
        $mn = "JwtAuth::getAuthorizationHeader()";
        AmsWadLogger::logBegin($mn);

        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }

        //AmsWadLogger::log($mn, "Header = " . (!empty($headers) ? "Empty" : " elements: " . count($headers)));
        AmsWadLogger::log($mn, "Header = " . $headers);
        AmsWadLogger::logEnd($mn);
        return $headers;
    }

    /**
     * get access token from header
     * */
    public static function getBearerToken() {
        $headers = JwtAuth::getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    private static function isTokenExpired($payload) {
        /*
          'iat' => $issuedAt, // Issued at: time when the token was generated
          'jti' => $jsonTokenId, // Json Token Id: an unique identifier for the token
          'iss' => $issuer, // Issuer
          'nbf' => $notBefore, // Not before
          'exp' => $expire, // Expire
         */
        $retValue = false;
        $mn = "JwtAuth::isTokenExpired()";
        AmsWadLogger::logBegin($mn);
        AmsWadLogger::log($mn, "iat:" . $payload->iat . ", exp:" . $payload->exp . ", nbf:" . $payload->nbf);
        //strtotime unix timestamp
        $now = time();
        $interval = $payload->exp - $now;
        AmsWadLogger::log($mn, "interval = " . $interval);
        AmsWadLogger::logEnd($mn);

        if ($interval < 0) {
            $retValue = true;
        }
        return $retValue;
    }

}
