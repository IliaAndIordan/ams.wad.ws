<?php

/*
 * AmsWadController is a Controller file that receives the request dispatches to respective methods 
 * to handle the request. 
 * 
 * ‘view’ key is used to identify the URL request.
 */
date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

//echo '\nAplication_home:'.APP_HOME.' \n';
ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
        $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
        $delim . '/home/iordanov/common/lib/ams' .
        $delim . '/home/iordanov/common/lib/ams/wad' .
        $delim . '/home/iordanov/common/lib/log4php' .
        $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
setcookie('cookiename', 'ws.ams.wad', time() + 60 * 60 * 24 * 365, '/', $domain, false);

//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "AmsWadController.php";
//--- Include CORS
require_once("rest_cors_header.php");

//require_once("AmsWadConnection.php");
require_once("AmsWadLogger.php");
require_once("Functions.php");
//---Handlers
require_once("AmsWadRegionRestHandler.class.php");
require_once("AmsWadCountryRestHandler.class.php");
require_once("AmsWadAirportEditRestHandler.class.php");
AmsWadLogger::logBegin($mn);


$view = "";
$id = null;
$iso2 = null;
$region = null;
$subregion = null;
if (isset($_REQUEST["view"]))
    $view = $_REQUEST["view"];
if (isset($_REQUEST["id"]))
    $id = $_REQUEST["id"];
if (isset($_REQUEST["iso2"]))
    $iso2 = $_REQUEST["iso2"];
if (isset($_REQUEST["region"]))
    $region = $_REQUEST["region"];
if (isset($_REQUEST["subregion"]))
    $subregion = $_REQUEST["subregion"];

AmsWadLogger::log($mn, "view=" . $view . ", id=" . $id);


if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new AmsWadAirportEditRestHandler();
    $restHendler->Option();
    AmsWadLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];

    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

        case "airports_edit":
            // to handle REST Url /pcpd/
            $restHendler = new AmsWadAirportEditRestHandler();
            if (isset($id)) {
                if ($id == "countbycountry") {
                    $restHendler->CountryStateStatusAirportCount();
                }
            } else
                AmsWadLogger::log($mn, "No heandler for view: " . $view . "->" . $id);
            break;
        case "region":
            // to handle REST Url /pcpd/
            $restHendler = new AmsWadRegionRestHandler();
            if (isset($id)) {
                if ($id == "countbycountry")
                    $restHendler->CountryStateStatusAirportCount();
            } else
                AmsWadLogger::log($mn, "No heandler for view: " . $view . "->" . $id);
            break;
        case "country":
            // to handle REST Url /mobile/show/<id>/
            $restHendler = new AmsWadCountryRestHandler();
            if (isset($id)) {
                $restHendler->County($id);
            } else if (isset($iso2)) {
                $restHendler->CountyByIso2($iso2);
            } else if (isset($region)) {
                $restHendler->CountyByRegion($region);
            } else if (isset($subregion)) {
                $restHendler->CountyBySubRegion($subregion);
            } else {
                $restHendler->CountyAll();
            }

            break;

        case "" :
            AmsWadLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}
AmsWadLogger::logEnd($mn);


