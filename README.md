# ams.wad.ws

Virtual World Airport Database Web Services.
Provides info for almost every airport in the world, including airport codes, abbreviations, runway lengths and other airport details.